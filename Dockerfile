FROM rust:latest as builder


WORKDIR /usr/src/my_actix_app


COPY . .


RUN cargo build --release


FROM debian:bookworm-slim



COPY --from=builder /usr/src/my_actix_app/target/release/my_actix_app .


RUN apt-get update \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*


EXPOSE 8080


CMD ["./my_actix_app"]
