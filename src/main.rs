use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
async fn greet() -> impl Responder {
    
    let mut rng = rand::thread_rng(); 
    let num1: i32 = rng.gen_range(0..101); 
    let num2: i32 = rng.gen_range(0..101); 
    let sum = num1 + num2; 

    HttpResponse::Ok().body(format!("Welcome to my app! The sum of {} and {} is {}", num1, num2, sum))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}


